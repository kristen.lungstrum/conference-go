from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    geo_params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    geo_response = requests.get(geo_url, params=geo_params)
    geo_content = json.loads(geo_response.content)
    try:
        lat = geo_content[0]["lat"]
        lon = geo_content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_response = requests.get(weather_url, params=weather_params)
    weather_content = json.loads(weather_response.content)
    try:
        return {
            "temp": weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return None
